/*Create a new empty set just as we did at the previous exercise on Lists
        • Add few elements on this set and print the elements of the Set
        • Create a list and add some elements
        • Now create the set using the appropriate constructor(in this case it’s going to be list)
        • Print the elements of the list and from the set
        • Compare the two sets using equals() method
        • Now we will remove one element from second set and compare again
        • Check if your sets contain all the elements of the list
        • Use the iterator in set and it will return true if the iteration has more elements
        • Print the next element in the iteration using next()
        • Clear all the elements from the set using clear() method and print it
        • Check the number of elements from sets and print it
        • Create an Array with the contents of the Set and print it*/


package Ro.Orange;

import java.util.*;

public class Main {

    public static void main(String[] args) {

    //Create a new empty set
	    Set<String> MyEmployeesSet1 = new HashSet<>();

	//Add few elements on this set and print the elements of the Set
        MyEmployeesSet1.add("Gheorghe Hagi");
        MyEmployeesSet1.add("Lionel Messi");
        MyEmployeesSet1.add("Cristiano Ronaldo");
        MyEmployeesSet1.add("Gaizka Mendieta");
        MyEmployeesSet1.add("Ruud van Nistelrooy");

        System.out.println("MyEmployeesSet1: " + MyEmployeesSet1.toString());

    //Create a list and add some elements
        List<String> list = new ArrayList<>();
        list.add ("Charlize Theron");
        list.add ("Meg Ryan");
        list.add ("Jennifer Connelly");
        list.add ("Jennifer Aniston");
        list.add ("Emma Watson");
        list.add ("Emma Stone");
        list.add ("Emma Stone");

    //Now create the set using the appropriate constructor(in this case it’s going to be list)
        Set<String> MyEmployeesSet2 = new HashSet<>(list);

    //Print the elements of the list and from the set
        System.out.println("list: " + list.toString());
        System.out.println("MyEmployeesSet2: " + MyEmployeesSet2.toString());

    //Compare the two sets using equals() method
        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " + MyEmployeesSet1.equals(MyEmployeesSet2));

    //Now we will remove one element from second set and compare again
        MyEmployeesSet2.remove("Meg Ryan");
        System.out.println("MyEmployeesSet2: " + MyEmployeesSet2.toString());
        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " + MyEmployeesSet1.equals(MyEmployeesSet2));

    //Check if your sets contain all the elements of the list
        System.out.println("myEmployeesSet1 contains all the elements: " + MyEmployeesSet1.equals(list));
        System.out.println("myEmployeesSet2 contains all the elements: " + MyEmployeesSet2.equals(list));

    //Use the iterator in set and it will return true if the iteration has more elements
        Iterator Em1iterator = MyEmployeesSet1.iterator();
        while (Em1iterator.hasNext()) {
            System.out.println("Iterator loop: " + Em1iterator.next());
        }

    //Clear all the elements from the set using clear() method and print it
        MyEmployeesSet1.clear();
        System.out.println("MyEmployeesSet1 is empty: "+MyEmployeesSet1.isEmpty());

    //Check the number of elements from sets and print it
        System.out.println("myEmployeesSet1 has: " + MyEmployeesSet1.size() + " Elements");
        System.out.println("myEmployeesSet2 has: " + MyEmployeesSet2.size() + " Elements");

    //Create an Array with the contents of the Set and print it
        String[] myArray = MyEmployeesSet2.toArray(new String[MyEmployeesSet1.size()]);
        System.out.println("The array: " + Arrays.toString(myArray));



    }
}
